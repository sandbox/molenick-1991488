Requirements: ImageMagick
Installation: Enable module.
How it works:
  Removes photo metadata for JPGs on file upload.

TODO:
Find and create list of appopriate mimetypes to scrub.
Create configurable option for re-adding geotags.
Consider which tags one could re-add.
Figure out how to make tag re-adding configurable - user, system, or both?
